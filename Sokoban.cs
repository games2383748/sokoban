using System;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

public class Sokoban {

    public static int LargeurSalle;
    public static int HauteurSalle;
    
    public static Panel[,] Cases;
    public static Carte[] levels;
    public static int level = 0;
    private const int Max_Level = 12;
    private static Panel BombCounter;
    private static Label count;

    public static Form Window;

    public static char key;

    public static bool KeyIsPress = false;

    public static Timer timer = new Timer();

    public static void Main(String[] args) {
        int num = 2;

        levels = new Carte[Max_Level];

        Console.WriteLine("Quelle mode voulez vous utiliser?");
        Console.WriteLine("1.Textuelle");
        Console.WriteLine("2.Graphique");
        do {
            while(!(int.TryParse(Console.ReadLine(), out num))) {Console.WriteLine("Entree incorect");}
        }
        while(num != 1 && num != 2);
        //Textuelle
        if(num == 1) {
            for(int i=level;i<Max_Level;i++) {
                levels[i] = new Carte(Recup_Code(i), i);

                levels[i].Affiche_Carte();

                while(!levels[i].Partie_Fini()) {
                    Console.Clear();
                    Console.WriteLine("Niveau "+ (i+1));
                    levels[i].Affiche_Carte();
                    Console.WriteLine("Entrez une direction");
                    Console.WriteLine("1.Haut");
                    Console.WriteLine("2.Bas");
                    Console.WriteLine("3.Gauche");
                    Console.WriteLine("4.Droite");
                    do {
                         while(!(int.TryParse(Console.ReadLine(), out num))) {Console.WriteLine("Entree incorect");}
                    }
                    while(num < 1 || num > 4);


                    levels[i].Move(num);
                }
            }
        }
        //Graphique
        else {
            levels[level] = new Carte(Recup_Code(level), level);
            Window = new Form();

            Window.KeyDown += new KeyEventHandler(KeyPress);
            Window.KeyUp += new KeyEventHandler(StopMove);
            Window.KeyPreview = true;

            Window.SizeChanged += new EventHandler(Resize);


            Window.WindowState = FormWindowState.Maximized;
            Cases = new Panel[HauteurSalle,LargeurSalle];

            timer.Interval = 70;
            timer.Tick += new EventHandler(timer_Tick);

            BombCounter = new Panel();
            BombCounter.Width = Window.Width / (HauteurSalle*2);
            BombCounter.Height = BombCounter.Width;
            BombCounter.BackgroundImage = Image.FromFile("Image/Back/Bomb.png");;
            BombCounter.BackgroundImageLayout = ImageLayout.Zoom;

            count = new Label();
            count.Text = "x0";
            count.Location = new Point(BombCounter.Width - count.Width, BombCounter.Height - count.Height);
            count.Margin = new Padding(0,0,0,0);
            count.BackColor = Color.Transparent;
            count.Font = new Font(count.Font.Name,count.Height);

            BombCounter.Controls.Add(count);



            Window.Controls.Add(BombCounter);


            

            for (int i = 0; i < HauteurSalle; i++) {
                for (int j = 0; j < LargeurSalle; j++) {
                    Cases[i,j] = new Panel();
                    Cases[i,j].Width = Window.Width / (HauteurSalle*2);
                    Cases[i,j].Height = Cases[i,j].Width;
                    Cases[i,j].Location = new Point(j*Cases[i,j].Width+Cases[i,j].Width/2, i*Cases[i,j].Width+Cases[i,j].Width/2);
                    Cases[i,j].BackgroundImage = levels[level].getImageFromChar(i,j);
                    Cases[i,j].BackgroundImageLayout = ImageLayout.Zoom;
                    Window.Controls.Add(Cases[i,j]);
                }
            }
            Window.ShowDialog();
        }
    }

    public static char[,] Recup_Code(int level) {
        string FilePath = "Binary/Carte/Level" + level + ".txt";
        
        string[] lignes = File.ReadAllLines(FilePath);
        HauteurSalle = lignes.Length;
        
        LargeurSalle = 0;
        for(int i=0;i<HauteurSalle;i++) {
            if(lignes[i].Length > LargeurSalle) {
                LargeurSalle = lignes[i].Length;
            }
        }

        char[,] tableau = new char[HauteurSalle,LargeurSalle];

        for(int i=0; i<HauteurSalle; i++) {
            for(int j=0; j<LargeurSalle; j++) {
                tableau[i,j] = lignes[i][j];
            }
        }

        return tableau;
    }

    public static void Load(int x, int y, int level) {
        Cases[x,y].BackgroundImage = levels[level].getImageFromChar(x,y);
    }

    private static void KeyPress(object sender, KeyEventArgs e) {
        if(!KeyIsPress) {
            KeyIsPress = true;
            if (char.IsLetterOrDigit(Convert.ToChar(e.KeyValue))) {
                key = Convert.ToChar(e.KeyValue);
                levels[level].Play(key);
                timer.Start();
                
            }
        }
    }
    private static void StopMove(object sender, KeyEventArgs e) {
        timer.Stop();
        KeyIsPress = false;
    }

    public static void Reload_Map() {
        levels[level] = new Carte(Recup_Code(level), level);
        SetBomb();

        for(int i=0;i<HauteurSalle;i++) {
            for(int j=0;j<LargeurSalle;j++) {
                Load(i, j, level);
            }
        }
    }

    private static void Resize(object sender, EventArgs e) {
        BombCounter.Width = Window.Width / (HauteurSalle*2);
        BombCounter.Height = BombCounter.Width;

        count.Location = new Point((int)(count.Width/2.8),(int)(count.Height*1.5));
        count.Font = new Font(count.Font.Name,count.Height/2);

        for (int i = 0; i < HauteurSalle; i++) {
            for (int j = 0; j < LargeurSalle; j++) {
                Cases[i,j].Width = Window.Width / (HauteurSalle*2);
                Cases[i,j].Height = Cases[i,j].Width;
                Cases[i,j].Location = new Point(j*Cases[i,j].Width+Cases[i,j].Width*2, i*Cases[i,j].Width);
            }
        }
    }

    public static void Next_Level() {
        level++;
        if(level >= Max_Level) {
            Window.Close();
            return;
        }

        ClearWindow();
        
        BombCounter = new Panel();
        BombCounter.Width = Window.Width / (HauteurSalle*2);
        BombCounter.Height = BombCounter.Width;
        BombCounter.BackgroundImage = Image.FromFile("Image/Back/Bomb.png");;
        BombCounter.BackgroundImageLayout = ImageLayout.Zoom;

        count = new Label();
        count.Text = "x0";
        count.Location = new Point(BombCounter.Width - count.Width, BombCounter.Height - count.Height);
        count.Margin = new Padding(0,0,0,0);
        count.BackColor = Color.Transparent;
        count.Font = new Font(count.Font.Name,count.Height);

        BombCounter.Controls.Add(count);
        Window.Controls.Add(BombCounter);
        
        levels[level] = new Carte(Recup_Code(level), level);

        Cases = new Panel[HauteurSalle,LargeurSalle];

        for (int i = 0; i < HauteurSalle; i++) {
            for (int j = 0; j < LargeurSalle; j++) {
                Cases[i,j] = new Panel();
                Cases[i,j].Width = Window.Width / (HauteurSalle*2);
                Cases[i,j].Height = Cases[i,j].Width;
                Cases[i,j].Location = new Point(j*Cases[i,j].Width+Cases[i,j].Width*2, i*Cases[i,j].Width);
                Cases[i,j].BackgroundImage = levels[level].getImageFromChar(i,j);
                Cases[i,j].BackgroundImageLayout = ImageLayout.Zoom;
                Window.Controls.Add(Cases[i,j]);
            }
        }
        Resize(Window,null);

        
        
        if(level < Max_Level) {
            Reload_Map();
        }
    }

    public static void SetBomb() {
        count.Text = "x"+levels[level].C4;
    }

    private static void ClearWindow() {
        while (Window.Controls.Count > 0) {
            Window.Controls[0].Dispose();
        }
    }

    private static void timer_Tick(object sender, EventArgs e) {
        levels[level].Play(key);
    }
}