using System;
using System.Windows.Forms;
using System.Drawing;
using System.Timers;

public class Carte {

    private char[,] CodeCarte;
    private int x;
    private int y;
    private int level;
    private Image[] caisse;
    private Image teleport;
    private Image but;
    private Image bomb;
    private Image mur;
    private Image murfragil;
    private Image sol;
    private Image[] joueur;
    private Image[] fleche;
    private Image reset;
    private int LastMove = 0;
    private Image[] lettre;
    private char saver = '#';
    private char saverplayer = '#';

    public int C4 = 0;

    public Carte(char[,] CodeCarte, int level) {
        this.CodeCarte = CodeCarte;
        this.level = level;

        this.joueur = new Image[4];
        this.caisse = new Image[2];
        this.fleche = new Image[4];

        this.lettre = new Image[26];

        for(int i=0;i<26;i++) {
            string FilePath = "Image/Back/Alphabet/"+((char)('A'+i)).ToString()+".gif";
            this.lettre[i] = Image.FromFile(FilePath);
        }
        this.but = Image.FromFile("Image/Back/but.gif");
        this.bomb = Image.FromFile("Image/Back/bomb.gif");
        this.teleport = Image.FromFile("Image/Back/teleport.gif");
        
        this.sol = Image.FromFile("Image/Back/sol.gif");
        this.mur = Image.FromFile("Image/Back/mur.gif");
        this.murfragil = Image.FromFile("Image/Back/murfragil.gif");
        
        this.caisse[0] = Image.FromFile("Image/Item/caisse1.gif");
        this.caisse[1] = Image.FromFile("Image/Item/caisse2.gif");
        
        this.joueur[0] = Image.FromFile("Image/Player/bas.gif");
        this.joueur[1] = Image.FromFile("Image/Player/haut.gif");
        this.joueur[2] = Image.FromFile("Image/Player/gauche.gif");
        this.joueur[3] = Image.FromFile("Image/Player/droite.gif");

        this.fleche[0] = Image.FromFile("Image/Back/Symbole/bas.gif");
        this.fleche[1] = Image.FromFile("Image/Back/Symbole/haut.gif");
        this.fleche[2] = Image.FromFile("Image/Back/Symbole/gauche.gif");
        this.fleche[3] = Image.FromFile("Image/Back/Symbole/droite.gif");
        this.reset = Image.FromFile("Image/Back/Symbole/reset.gif");


        for(int i=0;i<Sokoban.HauteurSalle;i++) {
            for(int j=0;j<Sokoban.LargeurSalle;j++) {
                if(this.CodeCarte[i,j] == '@') {
                    x=i;
                    y=j;
                }
            }
        }
    }




    public void Affiche_Carte() {
        for(int i=0;i<Sokoban.HauteurSalle;i++) {
            for(int j=0;j<Sokoban.LargeurSalle;j++) {
                Console.Write(CharToText(this.CodeCarte[i,j]));
            }
            Console.WriteLine();
        }
    }

    public bool Partie_Fini() {
        for(int i=0;i<Sokoban.HauteurSalle;i++) {
            for(int j=0;j<Sokoban.LargeurSalle;j++) {
                if(this.CodeCarte[i,j] == '.' || this.CodeCarte[i,j] == '+') {
                    return false;
                }
            }
        }
        return true;
    }

    public void Move(int dir) {
        int goX=-1, goY=-1;
        char sous, dest;

        if(dir == 5 && this.CodeCarte[this.x,this.y] == '|') {
            for(int i=0;i<Sokoban.HauteurSalle;i++) {
                for(int j=0;j<Sokoban.LargeurSalle;j++) {
                    if(this.CodeCarte[i,j] == '&') {
                        this.CodeCarte[i,j] = '|';
                        this.CodeCarte[this.x,this.y] = '&';
                        this.x = i;
                        this.y = j;
                        return;
                    }
                }
            }
        }
        
        switch(dir) {
            case 1: goX = this.x-1; goY = this.y; this.LastMove = 1; break;

            case 2: goX = this.x+1; goY = this.y; this.LastMove = 0; break;

            case 3: goX = this.x; goY = this.y-1; this.LastMove = 2; break;

            case 4: goX = this.x; goY = this.y+1; this.LastMove = 3; break;
        }


        if(this.CodeCarte[this.x,this.y] == '+') {
            sous = '.';
        }
        else if(this.CodeCarte[this.x,this.y] == '|'){
            sous = '&';
        }
        else {
            sous = ' ';
        }
        if(this.CodeCarte[goX,goY] == '.') {
            dest = '+';
        }
        else {
            dest = '@';
        }
            
        if(this.CodeCarte[goX,goY] != '#' && this.CodeCarte[goX,goY] != '*') {
            if(this.CodeCarte[goX,goY] == '$' || this.CodeCarte[goX,goY] == '£') {
                if(this.CodeCarte[goX+goX-x,goY+goY-y] != ' ' && this.CodeCarte[goX+goX-x,goY+goY-y] != '.' && this.CodeCarte[goX+goX-x,goY+goY-y] != '&') {
                    return;
                }
                else if(this.CodeCarte[goX+goX-x,goY+goY-y] == '.') {
                    this.CodeCarte[goX+goX-x,goY+goY-y] = '*';
                }
                else if(this.CodeCarte[goX+goX-x,goY+goY-y] == '&') {
                    this.CodeCarte[goX+goX-x,goY+goY-y] = '£';
                }
                else {
                    this.CodeCarte[goX+goX-x,goY+goY-y] = '$';

                }
            }
            if(this.CodeCarte[goX,goY] == '&' || this.CodeCarte[goX,goY] == '£') {
                dest = '|';
            }
            this.CodeCarte[goX,goY] = dest;
            
            this.CodeCarte[this.x,this.y] = sous;

            this.x = goX;
            this.y = goY;
        }
    }

    public void Move_Graphics(int dir) {
        bool test = false, push = false;
        int goX=-1, goY=-1;
        char sous=' ', dest='@', pusher='#';
        char here, go;


        
        
        switch(dir) {
            case 1: goX = this.x-1; goY = this.y; this.LastMove = 1; break;
            case 2: goX = this.x+1; goY = this.y; this.LastMove = 0; break;
            case 3: goX = this.x; goY = this.y-1; this.LastMove = 2; break;
            case 4: goX = this.x; goY = this.y+1; this.LastMove = 3; break;
            case 5: TeleportPlayer(); return;
            case 6: DestroyWall(); return;
        }
        go = this.CodeCarte[goX,goY];
        here = this.CodeCarte[this.x,this.y];


        switch(here) {
            case '+': sous = '.'; break;
            case '|': sous = '&'; break;
        }

        if(this.saverplayer != '#') {
            sous = this.saverplayer;
            this.saverplayer = '#';
        }


        switch(go) {
            case '#': 
            case '"': Sokoban.Load(this.x, this.y, this.level); return;
            case '.': dest = '+'; break;
            case '¤': this.C4++; Sokoban.SetBomb(); break;
            case '$': push = true; break;
            case '£': dest = '|'; push = true; break;
            case '*': dest = '+'; push = true; break;
            case '&': dest = '|'; break;
            case '§':
            case '<': 
            case '>':
            case '^':
            case ',': this.saverplayer = go; break;
        }
        
        if(Char.IsLetter(go)) {
            this.saverplayer = go;
        }
        
        if(push) {
            if(this.saver != '#') {
                this.saverplayer = this.saver;
                this.saver = '#';
            }

            switch(this.CodeCarte[goX+goX-this.x,goY+goY-this.y]) {
                case '#':
                case '$':
                case '£':
                case '"':
                case '*': Sokoban.Load(this.x, this.y, this.level); return;
                case ' ': pusher = '$'; break;
                case '&': pusher = '£'; break;
                case '.': pusher = '*'; test = true; break;
                case '¤': pusher = '$'; this.saver = '¤'; break;
                case '<': pusher = '$'; this.saver = '<'; break;
                case '>': pusher = '$'; this.saver = '>'; break;
                case '^': pusher = '$'; this.saver = '^'; break;
                case ',': pusher = '$'; this.saver = ','; break;
                default: pusher = '$'; this.saver = this.CodeCarte[goX+goX-this.x,goY+goY-this.y]; break;
            }
            this.CodeCarte[goX+goX-this.x,goY+goY-this.y] = pusher;
            Sokoban.Load(goX+goX-this.x, goY+goY-this.y, this.level);
        }

        this.CodeCarte[goX,goY] = dest;
        Sokoban.Load(goX, goY, this.level);
            
        this.CodeCarte[this.x,this.y] = sous;
        Sokoban.Load(this.x, this.y, this.level);

        this.x = goX;
        this.y = goY;
        
        
        Sokoban.Load(this.x, this.y, this.level);
        if(test && Partie_Fini()) {
            Sokoban.Next_Level();
        }
    }

    public void Play(char Key) {
        switch(Key) {
            case 'z': 
            case 'Z':  Move_Graphics(1);  break;

            case 'q': 
            case 'Q':   Move_Graphics(3);  break;

            case 's': 
            case 'S':   Move_Graphics(2);  break;

            case 'd': 
            case 'D':   Move_Graphics(4);  break;

            case 'r': 
            case 'R':   Sokoban.Reload_Map();     break;

            case 'E':
            case 'e':   Move_Graphics(5);   break;

            case 'a':
            case 'A':   Move_Graphics(6);   break;
        }
    }


    public Image getImageFromChar(int x, int y) {
        switch(this.CodeCarte[x,y]) {
            case ' ': return this.sol;
            case '.': return this.but;
            case '#': return this.mur;
            case '$': return this.caisse[0];
            case '£': return this.caisse[0];
            case '*': return this.caisse[1];
            case '@': return this.joueur[this.LastMove];
            case '+': return this.joueur[this.LastMove];
            case '|': return this.joueur[this.LastMove];
            case '"': return this.murfragil;
            case '¤': return this.bomb;
            case '&': return this.teleport;
            case '^': return this.fleche[1];
            case ',': return this.fleche[0];
            case '<': return this.fleche[2];
            case '>': return this.fleche[3];
            case '§': return this.reset;
            
        }

        if(Char.IsUpper(this.CodeCarte[x,y])) {
            return this.lettre[(int)(this.CodeCarte[x,y]-'A')];
        }
        else if(Char.IsLower(this.CodeCarte[x,y])) {
            return this.joueur[this.LastMove];
        }
        else {
            return null;
        }
    }

    private void TeleportPlayer() {
        if(this.CodeCarte[this.x,this.y] == '|') {
            for(int i=0;i<Sokoban.HauteurSalle;i++) {
                for(int j=0;j<Sokoban.LargeurSalle;j++) {
                    if(this.CodeCarte[i,j] == '&') {
                        this.CodeCarte[i,j] = '|';
                        this.CodeCarte[this.x,this.y] = '&';
                        Sokoban.Load(i,j, this.level);
                        Sokoban.Load(this.x,this.y, this.level);
                        this.x = i;
                        this.y = j;
                        return;
                    }
                    else if(this.CodeCarte[i,j] == '£') {
                        return;
                    }
                }
            }
        }
    }
    private void DestroyWall() {
        if(this.C4 == 0) {
            return;
        }
        switch(LastMove) {
            case 0: if(this.CodeCarte[this.x+1,this.y] != '"') {return;}
                    this.CodeCarte[this.x+1,this.y] = ' ';
                    Sokoban.Load(this.x+1,this.y, this.level);
            break;
            case 1: if(this.CodeCarte[this.x-1,this.y] != '"') {return;} 
                    this.CodeCarte[this.x-1,this.y] = ' ';
                    Sokoban.Load(this.x-1,this.y, this.level);
            break;
            case 2: if(this.CodeCarte[this.x,this.y-1] != '"') {return;} 
                    this.CodeCarte[this.x,this.y-1] = ' ';
                    Sokoban.Load(this.x,this.y-1, this.level);
            break;
            case 3: if(this.CodeCarte[this.x,this.y+1] != '"') {return;} 
                    this.CodeCarte[this.x,this.y+1] = ' ';
                    Sokoban.Load(this.x,this.y+1, this.level);
            break;
        }

        this.C4--;
        Sokoban.SetBomb();

        

    }

    private char CharToText(char c) {
        switch(c) {
            case '<':
            case '>':
            case '^':
            case ',': return ' ';
        }

        if(Char.IsUpper(c)) {
            return ' ';
        }
        else if(Char.IsLower(c)) {
            return '@';
        }
        return c;
    }
}